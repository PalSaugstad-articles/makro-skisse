Makro-skisse
============

.. raw:: html

   <!-- (php # 99 0 Makro-skisse) -->
   <!-- ($attrib_AC=8;) -->

======== ========
Av:      |author|
Versjon: |version|
Dato:    |date|
======== ========

.. include:: revision-header.rst



Utopi
-----

Jeg vil drøfte makro-økonomi i et utopi-perspektiv, altså at man først ser for seg hva målet for makro-skissen,
samtidig som man er seg bevisst om at verden faktisk ikke vil bevege seg helt dit.
Formålet er å finne ut hvilke grep en må gjøre idag for å nærme seg utopien - litt.

Hvilken utopi ønsker vi oss?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For noen er den ultimate utopien at menneskeheten skal få fullstendig oversikt over alle tilværelsens aspekter,
være i stand til å utforske det fysiske universet helt fra elemementær-partikler til den fjerneste planet, finne liv på andre planeter,
reise til andre planeter, kunne bosette seg på andre planeter. Eller for å si det med færre ord, lære seg alt, og ekspandere til hele universet.

Jeg hører til dem som har litt mindre ambisjoner. Jeg tror ikke det er noe mål å lære seg 'alt'.
Enhver ny ting man lærer, og behersker,
avdekker nye ting som man ikke tenkte på som et tema engang. Heller ikke menneskesinnet tror jeg at man noen gang helt kommer til å forstå.
Det er med disse temaene som med politikk: Man prøver å lære seg og beherske et system man selv er en del av.
Det er vanskelig, ja antakeligvis umulig, å få full oversikt over ens eget bevegelige system.

Mine basis-aksiomer og/eller de utopiske målene
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Mennesker er tett knyttet til jorden.
  Det er langt verre å leve alle andre steder, både psykisk og fysisk.
- Det som virkelig teller for livs-gleden, er relasjoner mellom mennesker, og mellom mennesket og naturen/kulturen.
- Vi må lære oss å fordele rettferdig de resursene som finnes
- Vekst i form av flere varer og tjenester pr. person blir en ulempe etter et visst nivå (ref. Herman Daly og Solow).
- Befolknings-begrensning er viktigere en "riktig" demografi
- Utvikling av nye metoder, produkter og tjenester er en fin ting, men det er ingen katastrofe om utviklingstakten går saktere.

Vi bør ikke søke etter å oppnå livsforlengelse: En viktig del av livsgleden er at det finnes barn. Dersom hvert enkelt menneske blir ut-gammelt,
blir det desto mindre rom til barn.

Sentrale begreper
-----------------

Mange kjente effekter bør behandles i parallell når man skal se for seg hvordan samfunnsutviklingen kommer til å bli,
og ikke minst,
i hvilken retning vi må endre politikk og endre samfunns-strukturene for å komme nærmere utopien.


Kjente effekter og mekanismer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Solow-modellen (kapitalslitet gjør at det blir vanskeligere å vokse etterhvert som man allerede har vokst lenge)
- Wagners lov (Politisk press for økt velferd som funksjon av industrisamfunnets framvekst)
- Baumols sykdom (Arbeidsintensive yrker (som velferd) koster relativt sett mer og mer som funksjon av økonomisk vekst)
- Landets import/eksport-balanse i forhold til landets gjeld/formue overfor utlandet
- Demografi (Økt levealder og lavere barne-antall gir en økning i den eldre delen av befolkningen)

Ikke så kjente effekter og mekanismer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- MMT (Modern Money Theory, penger skapes når private banker yter lån)
- Sigbjørn Atle Bergs penge-lekkasje (Penger lekker ut av pengeholdende sektor når denne kjøper
  verdipapier fra annen sektor, uten at lånemengden i pengeholdende sektor reduseres tilsvarende)

Mekanismer som det er delte meninger om
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Skattelettelser
- Pikettys ulikhets-formel (Hvis real-renten er høyere enn vekst-raten i økonomien, øker ulikheten)
- Å ta risiko må føre til at man får fortjeneste (reformulér dette)

Mekanismer som er på forsøksstadiet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Bancor/ICU (Felles verdensvaluta som alle vekslings-transaksjoner går via)
- Borgerlønn (UBI)
- Jobbgaranti
- Kortere arbeidstid

Effekter jeg antar er tilfelle, men som jeg ikke har sett beskrevet noe sted
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Teori om avkasning på verdipapier (En kan oppnå makro-messig positiv avkastning dersom man har generell økonomisk vekst, eller dersom ulikheten øker)
- Trender som fører til negativ vekst
  - Matkastelov
  - Minimalisme (liten boflate bl. a.)
  - Bil-deling og Airbnb
  - Energi- og material-effektivisering (ref. Per Espen Stoknes, rasjonalisere bort en stor overflødig vare- og energi-strøm)


Min forståelse av Perspektivmeldingen
-------------------------------------

Perspektivmelingene til Solberg-gjereringen (2017) drøfter endel effekter og mekanismer. Konklusjonene blir tildels selvmotsigende:
Vi må regne med at det blir vanskeligere å få budsjettene til å gå opp,
og at det dermed antakelig blir et redusert helsetilbud og færre midler til syketrygd og pensjon.
Skjevere demografi, robotisering, flere som hever syketrygd - mange tendenser som tilsynelatende gjør at færre hender må løfte flere mennesker.
Man antar at det man må gjøre for å demme opp for dette er å effektivisere offentlig sektor, innbefattet sykehus og velferd som barnehage,
eldresentre, hjemmehjelp o.l.

Selvmotsigelsen går på at det legges til grunn en antatt økonomisk vekst, men at man konkluderer med at alt som normalt sett
skal bli bedre når vi har økonomisk vekst blir værre.
I tillegg har man en sterk antakelse om at det å bidra til at de svakest stilte skal øke sin velstand, så må dette gjøres indirekte ved å øke
de økonomiske mulighetene til de rikeste først (skattelette),
slik at disse kan bidra med å skape nye arbeidsplasser slik at velstanden sildrer nedover i samfunnet etter hvert.
Man ser også for seg at samfunnet skal være i stadig endring, og at mer og mer kunnskap er nødvendig hos alle som vil klare seg.
Man snakker om livslang læring, og slik jeg forstår det, dreier dette seg om læring i form av videreutdanning for å være istand til å være aktuelle
arbeidstakere i et stadig mer krevende arbeidsliv.

Vanlig måte å se på automatisering av industrien på er jo
at de arbeidstakerene som blir frigjort,
fort kommer seg over i andre yrker fordi det er en uendelighet
av oppgaver som vi ønsker å løse.
I perspektivmeldingen og samfunnsdebatten generelt er det en dreining nå
over til å ikke være så sikker lenger på at det faktisk er en uendelighet av oppgaver:
Mange slike oppgaver kan jo i utgangspunktet gjøres automatisk.
Det som står igjen er kreative yrker,
dvs. ikke-trivielle oppgaver
som krever logisk evne og sammensetting av informasjon
som det (foreløpig) ikke synes å være trivielt å lage en computer-erstatning for (kunstig intellegens).
Det er disse oppgavene som kommer til å våre i stadig endring, slik at livslang læring blir en nødvendighet

(Fortettelse kommer!)

Andre ting
----------


* `Fred Hirsch, Social limits to growth 1976
  <https://zielonygrzyb.wordpress.com/2012/01/22/social-limits-to-growth/>`_



.. raw:: html

   <hr>
   <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Makro-skisse">Tweet</a>
   <br>
   <a target="_blank" href="show?f=articles/parsed/makro-skisse/nor/makro-skisse.pdf">som pdf</a>
